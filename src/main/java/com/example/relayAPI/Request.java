package com.example.relayAPI;

import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Request {

    private int requestId;
    private String requestName;
    private String testStatus;
    private String collectionName;
    private String environmentName;
    private Response response;
    private ArrayList<Assertion> assertions;




    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public String getRequestName() {
        return requestName;
    }

    public void setRequestName(String requestName) {
        this.requestName = requestName;
    }

    public String getTestStatus() {
        return testStatus;
    }

    public void setTestStatus(String testStatus) {
        this.testStatus = testStatus;
    }

    public String getCollectionName() {
        return collectionName;
    }

    public void setCollectionName(String collectionName) {
        this.collectionName = collectionName;
    }

    public String getEnvironmentName() {
        return environmentName;
    }

    public void setEnvironmentName(String environmentName) {
        this.environmentName = environmentName;
    }

    public ArrayList<Assertion> getAssertions() {
        return assertions;
    }

    public void setAssertions(ArrayList<Assertion> assertions) {
        this.assertions = assertions;
    }

    public List<Map<String, Object>> getAssertionData(int requestID) {
        Statement stmt = null;
        ResultSet rs = null;
        List<Map<String, Object>> result = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //String connectionUrl = "jdbc:sqlserver://DESKTOP-7534RDA;databaseName=RELAYResultsJmeterPOC;integratedSecurity=true";
            String connectionUrl = "jdbc:sqlserver://DESKTOP-7534RDA;user=relaytapQA;password=Password1;";


            Connection conn = DriverManager.getConnection(connectionUrl);


            stmt = conn.createStatement();
            String sql;
            sql = "SELECT RequestID, AssertionName, Failure, Error, FailureMessage " +
                    "FROM Assertion Where RequestID = " + requestID;



            rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set


            result = new MapListHandler().handle(rs);



            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();


        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        return result;

    }

    public Request(Map<String, Object> result){
        setRequestId( Integer.parseInt(result.get("RequestId").toString())   );
        setRequestName(result.get("RequestName").toString());
        setTestStatus(result.get("TestStatus").toString());
        setCollectionName(result.get("CollectionName").toString());
        setEnvironmentName(result.get("EnvironmentName").toString());


        //get assertions
        List<Map<String, Object>> getAssertionsResult = getAssertionData(Integer.parseInt(result.get("RequestId").toString()) );
        ArrayList<Assertion> assertions = new ArrayList<>();

       for (Map<String, Object> stringObjectMap : getAssertionsResult) {

            assertions.add(new Assertion(stringObjectMap));
        }

        setAssertions(assertions);

       //get response
        List<Map<String, Object>> responseResult = getResponseData(Integer.parseInt(result.get("RequestId").toString()) );
        Response response = new Response(responseResult.get(0));
        setResponse(response);
    }

    public List<Map<String, Object>> getResponseData(int requestID) {
        Statement stmt = null;
        ResultSet rs = null;
        List<Map<String, Object>> result = null;

        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //String connectionUrl = "jdbc:sqlserver://DESKTOP-7534RDA;databaseName=RELAYResultsJmeterPOC;integratedSecurity=true";
            String connectionUrl = "jdbc:sqlserver://DESKTOP-7534RDA;user=relaytapQA;password=Password1;";


            Connection conn = DriverManager.getConnection(connectionUrl);


            stmt = conn.createStatement();
            String sql;
            sql = "SELECT ResponseId, RequestID, ResponseCode, " +
                    "ResponseMessage, TimeInSeconds " +
                    "FROM Response Where RequestID = " + requestID;



            rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set


            result = new MapListHandler().handle(rs);



            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();


        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        return result;

    }


    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }
}
