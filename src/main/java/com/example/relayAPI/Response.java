package com.example.relayAPI;

import java.util.Map;

public class Response {

    private int responseID;
    private int requestID;
    private String responseCode;
    private String responseMessage;
    private int timeInSeconds;

    public Response(Map<String, Object> result) {
        setResponseID(Integer.parseInt(result.get("ResponseId").toString()));
        setRequestID(Integer.parseInt(result.get("RequestId").toString()));
        setResponseCode(result.get("ResponseCode").toString());
        setResponseMessage(result.get("ResponseMessage").toString());
        setTimeInSeconds(Integer.parseInt(result.get("TimeInSeconds").toString()));
    }


    public int getResponseID() {
        return responseID;
    }

    public void setResponseID(int responseID) {
        this.responseID = responseID;
    }

    public int getRequestID() {
        return requestID;
    }

    public void setRequestID(int requestID) {
        this.requestID = requestID;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public void setResponseMessage(String responseMessage) {
        this.responseMessage = responseMessage;
    }

    public int getTimeInSeconds() {
        return timeInSeconds;
    }

    public void setTimeInSeconds(int timeInSeconds) {
        this.timeInSeconds = timeInSeconds;
    }
}
