package com.example.relayAPI;

import org.apache.commons.dbutils.handlers.MapListHandler;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Collection {

    private ArrayList<Request> requests;

    public Collection(String collection, String environment){

        List<Map<String, Object>> result = getRequestData(collection,environment);

        ArrayList<Request> requests = new ArrayList<>();

        for (Map<String, Object> stringObjectMap : result) {
            requests.add(new Request(stringObjectMap));
        }

        setRequests(requests);
    }



    public List<Map<String, Object>> getRequestData(String collection, String environment) {
        Statement stmt = null;
        ResultSet rs = null;
        List<Map<String, Object>> result = null;


        try {
            Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            //String connectionUrl = "jdbc:sqlserver://DESKTOP-7534RDA;databaseName=RELAYResultsJmeterPOC;integratedSecurity=true";
            String connectionUrl = "jdbc:sqlserver://DESKTOP-7534RDA;user=relaytapQA;password=Password1;";


            Connection conn = DriverManager.getConnection(connectionUrl);


            stmt = conn.createStatement();
            String sql;
            sql = "SELECT RequestId" +
                    ", RequestName" +
                    ", CASE WHEN TestStatus = 1 THEN 'Pass' ELSE 'Fail' END as TestStatus" +
                    ", CollectionName" +
                    ", EnvironmentName " +
                    "FROM Request a join Collection b on a.CollectionID = b.CollectionID "+
                    "join Environment c on a.EnvironmentID = c.EnvironmentID " +
                    "WHERE CollectionName = " + "'" + collection + "'" + " and EnvironmentName = " + "'" + environment + "'";



            rs = stmt.executeQuery(sql);

            //STEP 5: Extract data from result set


            result = new MapListHandler().handle(rs);


            //STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();


        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
        return result;
    }


    public ArrayList<Request> getRequests() {
        return requests;
    }

    public void setRequests(ArrayList<Request> requests) {
        this.requests = requests;
    }
}
