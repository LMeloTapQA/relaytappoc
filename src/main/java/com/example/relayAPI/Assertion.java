package com.example.relayAPI;


import java.util.Map;

public class Assertion {

    private int requestId;
    private String assertionName;
    private String failure;
    private String error;
    private String failureMessage;



    public Assertion(Map<String, Object> result){

        setRequestId(Integer.parseInt(result.get("RequestId").toString()));
        setAssertionName(result.get("AssertionName").toString());
        setFailure(result.get("Failure").toString());
        setError(result.get("Error").toString());
        setFailureMessage(result.get("FailureMessage").toString());
    }

    public Assertion(int requestId)
    {
        System.out.println("Unused Assertion constructor");
    }


    public String getAssertionName(){
        return assertionName;
    }

    public String getFailure() {
        return failure;
    }

    public void setFailure(String failure) {
        this.failure = failure;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getFailureMessage() {
        return failureMessage;
    }

    public void setFailureMessage(String failureMessage) {
        this.failureMessage = failureMessage;
    }



    public int getRequestId() {
        return requestId;
    }

    public void setRequestId(int requestId) {
        this.requestId = requestId;
    }

    public void setAssertionName(String assertionName) {
        this.assertionName = assertionName;
    }
}
