package com.example.relayAPI;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CollectionController {

    @GetMapping("/collection")
    public Collection collection (@RequestParam(value = "CollectionName")String collection,
                            @RequestParam(value = "Environment", defaultValue = "Production")String environment )  {
        return new Collection(collection, environment);


    }
}
