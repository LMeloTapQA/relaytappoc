package com.example.relayAPI;


/*
spring.jpa.hibernate.ddl-auto=none
spring.datasource.url=jdbc:sqlserver://DESKTOP-7534RDA;databaseName=RELAYResultsJmeterPOC
spring.datasource.username=relaytapQA
spring.datasource.password=Password1
*/

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelayAPI {

    public static void main(String[] args) {
        SpringApplication.run(RelayAPI.class, args);
    }


}
