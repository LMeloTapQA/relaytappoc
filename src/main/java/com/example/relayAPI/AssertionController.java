package com.example.relayAPI;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AssertionController {


    /**
     *
     * This one doesn't do anything, it may not be needed
     */
    @GetMapping("/assertion")
    public Assertion assertion(@RequestParam(value = "RequestId", defaultValue = "783")String requestId){
        return new Assertion(Integer.parseInt(requestId));

    }
}
